/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mario_pt1.pkg21;

/**
 *
 * @author stevenrolf
 */
public class NewClass2 {
    private String Name;
    private int Telephone;

    public NewClass2() {
    }
    
    public NewClass2(String Name, int Telephone) {
        this.Name = Name;
        this.Telephone = Telephone;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getTelephone() {
        return Telephone;
    }

    public void setTelephone(int Telephone) {
        this.Telephone = Telephone;
    }

    @Override
    public String toString() {
        return "NewClass2{" + "Name=" + Name + ", Telephone=" + Telephone + '}';
    }
    
    
    
}
