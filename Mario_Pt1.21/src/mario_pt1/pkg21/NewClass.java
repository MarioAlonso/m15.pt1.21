/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mario_pt1.pkg21;

/**
 *
 * @author stevenrolf
 */
public class NewClass {
    String name;
    String code;

    public NewClass(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("NewClass{name=").append(name);
        sb.append(", code=").append(code);
        sb.append('}');
        return sb.toString();
    }
    
    
    
}

